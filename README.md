# Linux Learning Experiment | WIP
#
Ce répertoire **FramaGIT** me servira d'entrepôt, celui-ci sera concentré sur mon avancé Linux (Ubuntu, et autres distributions), et inclura des petites astuces, un peu de programmmation **Python** et **BASH** 

# Qui suis-je et quand ai-je eu l'envie de devenir linuxien ? 

Je suis un collégien de 15 ans qui participe activement à la découverte de **libre**. J'ai eu l'envie de devenir linuxien suite aux récentes découvertes sur la NSA d'**[Edward Snowden](https://git.chevro.fr/Eban/Snowden-summary-mirror)**
Les GAFAMS ont fait parti de ma vie, depuis 2019, je n'utilise qu'Amazon, et le compte mail de ma mère qui est à son nom.


# SOON

Ça arrive ! 
